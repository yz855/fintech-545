All the libraries you need are listed on the first part of the code.
Here also provide the libraries:

* import pandas as pd
* import numpy as np
* from scipy.stats import t, norm
* from scipy.stats import spearmanr

You need to include the risk_management.py (My Library for risk management)
You may change the directory according to the specific situation. E.g. sys.path.append('../')
* import sys
* sys.path.append('../../')
* from risk_management import return_calculate, simulate_pca

Regardless of which question's code you intend to execute, you need to first run the code in the first section of the Jupyter notebook to import the libraries.

# Notice: 
1. To test my Library for risk management, you need to uncomment the test section, than run the code in your terminal.
2. If you run the Week05_Code.ipynb after running the risk_management.py, you need to comment the test section again, in order to include the function in risk_management.py to Week05_Code.ipynb appropriately.