import pandas as pd
import numpy as np

from numpy.random import default_rng
from scipy.stats import t, norm, kurtosis
from scipy.optimize import minimize
from scipy.stats import spearmanr
from scipy.integrate import quad
from pyomo.environ import ConcreteModel, Var, Objective, SolverFactory, value, maximize, log
from statsmodels.tsa.arima.model import ARIMA
import statsmodels.api as sm

# Covariance estimation techniques
def missing_cov(x, skipMiss=True, fun = np.cov):
    n, m = x.shape
    
    if skipMiss:
        rows = set(range(n))
        for c in range(m):
            for rm in np.where(np.isnan(x[:, c]))[0]:
                rows.discard(rm)
        rows = sorted(list(rows))
        return fun(x[rows, :], rowvar=False)
    else:
        out = np.empty((m, m))
        for i in range(m):
            for j in range(i + 1):
                rows = set(range(n))
                for c in (i, j):
                    for rm in np.where(np.isnan(x[:, c]))[0]:
                        rows.discard(rm)
                rows = sorted(list(rows))
                cov = fun(x[rows][:, [i, j]], rowvar=False)
                out[i, j] = cov[0, 1]
                if i != j:
                    out[j, i] = cov[1, 0]
        return out

def ewCovar(x, lam):
    w = np.empty(len(x))

    # Remove the mean from the series
    xm = x.mean(axis=0)
    x1 = x - xm

    # Calculate weights
    for i in range(len(x)):
        w[i] = (1 - lam) * lam**(len(x) - i - 1)

    # Normalize weights to 1
    w /= w.sum()
    w = w[:, np.newaxis]
    
    # Covariance[i,j] = (w * x)' @ x, where * is elementwise multiplication
    return (w * x1).T @ x1

def ewCor(x, lam):
    w = np.empty(len(x))

    # Remove the mean from the series
    xm = x.mean(axis=0)
    x1 = x - xm

    # Calculate weights
    for i in range(len(x)):
        w[i] = (1 - lam) * lam**(len(x) - i - 1)

    # Normalize weights to 1
    w /= w.sum()
    w = w[:, np.newaxis]
    
    out = (w * x1).T @ x1
    invSD = np.diag(1.0 / np.sqrt(np.diag(out)))
    out = invSD @ out @ invSD
    
    return out

def ewCovarCor(x, lamCovar, lamCor):
    outCovar = ewCovar(x, lamCovar)
    outCor = ewCor(x, lamCor)
    
    invSD = np.diag(np.sqrt(np.diag(outCovar)))
    out = invSD @ outCor @ invSD
    
    return out

# Let all eigenvalues be greater than or equal to epsilon
def near_psd(a, epsilon=0.0):
    n = a.shape[0]

    invSD = None
    out = a.copy()

    # calculate the correlation matrix if we got a covariance matrix
    if np.count_nonzero(np.isclose(np.diag(out), 1.0)) != n:
        invSD = np.diag(1.0 / np.sqrt(np.diag(out)))
        out = invSD @ out @ invSD

    # SVD, update the eigenvalue and scale
    vals, vecs = np.linalg.eigh(out)
    vals = np.maximum(vals, epsilon)
    vals = vals[:, np.newaxis]
    T_diagonal_scaling_matrix = 1.0 / (vecs * vecs @ vals)
    T_diagonal_scaling_matrix = np.sqrt(np.diag(T_diagonal_scaling_matrix.flatten()))
    l = np.diag(np.sqrt(vals.flatten()))
    B_matrix = T_diagonal_scaling_matrix @ vecs @ l
    out = B_matrix @ B_matrix.T

    # Add back the variance
    if invSD is not None:
        invSD = np.diag(1.0 / np.diag(invSD))
        out = invSD @ out @ invSD

    return out

# Higham method
def _getAplus(A):
    vals, vecs = np.linalg.eig(A)
    vals = np.diag(np.maximum(vals, 0))
    return vecs @ vals @ vecs.T

def _getPS(A, W):
    W_sqrt = np.sqrt(W)
    iW = np.linalg.inv(W_sqrt)
    return iW @ _getAplus(W_sqrt @ A @ W_sqrt) @ iW

def _getPu(A, W):
    Aret = A.copy()
    np.fill_diagonal(Aret, 1.0)
    return Aret

def wgtNorm(A, W): # Frobenius Norm
    W_sqrt = np.sqrt(W)
    A_Weight = W_sqrt @ A @ W_sqrt
    return np.real(np.sum(A_Weight * A_Weight))

def higham_nearestPSD(pc, W=None, epsilon=1e-9, maxIter=200, tol=1e-9):
    n = pc.shape[0]
    if W is None:
        W = np.diag(np.ones(n))
    
    invSD = None
    Yk = pc.copy()
    
    # calculate the correlation matrix if we got a covariance matrix
    if np.count_nonzero(np.isclose(np.diag(Yk), 1.0)) != n:
        invSD = np.diag(1.0 / np.sqrt(np.diag(Yk)))
        Yk = invSD @ Yk @ invSD

    deltaS = 0
    
    norml = np.finfo(float).max
    i = 1

    while i <= maxIter:
        Rk = Yk - deltaS
        # Ps Update
        Xk = _getPS(Rk, W)
        deltaS = Xk - Rk
        # Pu Update
        Yk = _getPu(Xk, W)
        # Get Norm
        norm = wgtNorm(Yk - pc, W)
        # Smallest Eigenvalue
        minEigVal = np.min(np.real(np.linalg.eigvals(Yk)))

        if np.abs(norm - norml) < tol and minEigVal > -epsilon:
            # Norm converged and matrix is at least PSD
            break

        norml = norm
        i += 1

    if i < maxIter:
        print(f"Converged in {i} iterations.")
    else:
        print(f"Convergence failed after {i-1} iterations")

    # Add back the variance
    if invSD is not None:
        invSD = np.diag(1.0 / np.diag(invSD))
        Yk = invSD @ Yk @ invSD
    
    return Yk

def chol_psd(a):
    # Initialize the root matrix with 0 values
    root = np.zeros_like(a)
    
    n = a.shape[0]

    # Loop over columns
    for j in range(n):
        s = 0.0

        # If we are not on the first column, calculate the dot product of the preceding row values.
        if j > 0:
            s = np.dot(root[j, :j], root[j, :j])

        # Diagonal Element
        temp = a[j, j] - s
        if 0 >= temp >= -1e-8:
            temp = 0.0
        root[j, j] = np.sqrt(temp)

        # Check for the 0 eigenvalue. Just set the column to 0 if we have one
        if 0.0 == root[j, j]:
            root[j, (j + 1):] = 0.0
        else:
            # Update off-diagonal rows of the column
            ir = 1.0 / root[j, j]
            for i in range(j + 1, n):
                s = np.dot(root[i, :j], root[j, :j])
                root[i, j] = (a[i, j] - s) * ir
                
    return root

def simulate_normal(N, cov, mean=[], seed=1234):
    n, m = cov.shape
    if n != m:
        raise ValueError(f"Covariance Matrix is not square ({n},{m})")

    out = np.empty((n, N))

    _mean = np.zeros(n)
    if mean:
        m = len(mean)
        if n != m:
            raise ValueError(f"Mean ({m}) is not the size of cov ({n},{n})")
        _mean = np.array(mean)

    l = chol_psd(cov)

    rng = default_rng(seed)
    d = rng.normal(size=(n, N))

    #apply the standard normals to the cholesky root
    out = np.dot(l, d).T

    for i in range(n):
        out[:, i] += _mean[i]

    return out

def simulate_pca(a, nsim, pctExp=1.0, mean=[], seed=1234):
    n = a.shape[0]

    # If the mean is missing then set to 0, otherwise use provided mean
    _mean = np.zeros(n)
    if mean:
        _mean = np.array(mean)

    # Eigenvalue decomposition
    vals, vecs = np.linalg.eigh(a)
    vals = np.real(vals)
    vecs = np.real(vecs)

    # sort high-low
    flip = np.arange(vals.size - 1, -1, -1)
    vals = vals[flip]
    vecs = vecs[:, flip]

    tv = np.sum(vals)

    posv = np.where(vals >= 1e-8)[0]
    if pctExp < 1:
        nval = 0
        pct = 0.0
        # figure out how many factors we need for the requested percent explained
        for i in posv:
            pct += vals[i] / tv
            nval += 1
            if pct >= pctExp:
                break
        if nval < posv.size:
            posv = posv[:nval]
    vals = vals[posv]
    vecs = vecs[:, posv]

    # Simulating with specified PC Factors
    B_matrix = vecs @ np.diag(np.sqrt(vals))

    rng = default_rng(seed)
    m = vals.size
    r = rng.standard_normal((m, nsim))

    out = np.dot(B_matrix, r).T
    # Loop over iterations and add the mean
    for i in range(n):
        out[:, i] += _mean[i]
    return out

def return_calculate(prices, method="DISCRETE", dateColumn="date"):
    vars = list(prices.columns)
    nVars = len(vars)
    vars = [var for var in vars if var != dateColumn]
    
    if nVars == len(vars):
        raise ValueError(f"dateColumn: {dateColumn} not in DataFrame: {vars}")

    nVars -= 1

    p = prices[vars].values
    n, m = p.shape
    p2 = np.empty((n - 1, m), dtype=np.float64)

    for i in range(n - 1):
        for j in range(m):
            p2[i, j] = p[i + 1, j] / p[i, j]

    if method.upper() == "DISCRETE":
        p2 -= 1.0
    elif method.upper() == "LOG":
        p2 = np.log(p2)
    else:
        raise ValueError(f"method: {method} must be in (\"LOG\", \"DISCRETE\")")

    dates = prices[dateColumn].iloc[1:].reset_index(drop=True)       
    out_dict = {dateColumn: dates}
    for i, var in enumerate(vars):
        out_dict[var] = p2[:, i]

    out = pd.DataFrame(out_dict)

    return out

class FittedModel:
    def __init__(self, beta, error_model, eval_func, errors, u):
        self.beta = beta
        self.error_model = error_model
        self.eval = eval_func
        self.errors = errors
        self.u = u

def neg_log_likelihood_t(params, x, y):
    mu, s, nu, *b = params
    b = np.array(b)[np.newaxis, :]
    xm = y - np.sum(np.hstack((np.ones((x.shape[0], 1)), x)) * b, axis=1).reshape(x.shape[0], 1)
    log_likelihood = np.sum(t.logpdf(x=xm, df=nu, loc=mu, scale=s))
    return -log_likelihood

def fit_regression_t(y, x):
    n = x.shape[0]
    # Approximate values based on moments and OLS
    b_start = np.linalg.lstsq(np.hstack((np.ones((n, 1)), x)), y, rcond=None)[0]
    e = y - np.sum(np.hstack((np.ones((n, 1)), x)) * b_start.T, axis=1).reshape(n, 1)
    start_m = np.mean(e.flatten())
    start_nu = 6.0 / kurtosis(e.flatten()) + 4
    start_s = np.sqrt(np.var(e.flatten()) * (start_nu - 2) / start_nu)

    # Define the initial guess for the parameters
    initial_params = np.concatenate([[[start_m]], [[start_s]], [[start_nu]], b_start]).flatten()
    # Define bounds for the parameters
    bounds = [(None, None), (1e-6, None), (2.0001, None)] + [(None, None)] * (x.shape[1] + 1)

    # Minimize the negative log-likelihood function
    result = minimize(neg_log_likelihood_t, initial_params, args=(x, y), bounds=bounds)

    # Retrieve optimized values
    m, s, nu, *beta = result.x

    # Define the fitted error model
    error_model = t(df=nu, loc=m, scale=s)

    # Function to evaluate the model for a given x and u
    def eval_model(x, u):
        n = x.shape[0]
        _temp = np.hstack((np.ones((n, 1)), x))
        return np.dot(_temp, beta) + error_model.ppf(u) # add estimated error

    # Calculate the regression errors and their U values
    errors = y - eval_model(x, np.full((x.shape[0],), 0.5))
    u = error_model.cdf(errors)

    return nu, m, s, FittedModel(beta, error_model, eval_model, errors, u)

def fit_general_t(x):
    global __x 
    __x = x
    # Create a concrete model
    mle = ConcreteModel()
    
    # approximate values based on moments
    start_m = np.mean(x)
    start_nu = 6.0 / kurtosis(x) + 4
    start_s = np.sqrt(np.var(x) * (start_nu - 2) / start_nu)

    # Variables
    mle.m = Var(initialize=start_m)
    mle.s = Var(bounds=(1e-6, None), initialize=1)
    mle.nu = Var(bounds=(2.0001, None), initialize=start_s)

    # Inner function to abstract away the X value
    def _gtl(mu, s, nu, x):
        return sum(log(t.pdf(xi, nu, loc=mu, scale=s)) for xi in __x)

    # Objective function
    def obj_rule(model):
        return -_gtl(model.m, model.s, model.nu)
    mle.obj = Objective(rule=obj_rule, sense=maximize)

    # Solve
    SolverFactory('ipopt').solve(mle)

    # Retrieve optimized values
    m = value(mle.m)
    s = value(mle.s)
    nu = value(mle.nu)

    # Return the parameters as well as the Distribution Object
    return m, s, nu, t(df=nu, loc=m, scale=s)

def mc_var(returns, method='ABS'): 
    n = 10000
    mean = np.mean(returns)
    # Using a normal distribution
    if method == 'ABS':
        sim_returns = np.random.normal(mean, np.sqrt(np.var(returns)), n)
    else:
        returns_new = returns - mean
        sim_returns = np.random.normal(0, np.sqrt(np.var(returns_new)), n)
    pRet = np.sort(sim_returns)
    
    # Calculate MC VaR
    aup = int(np.ceil(0.05 * n))
    adn = int(np.floor(0.05 * n))
    VaR = (pRet[aup] + pRet[adn]) / 2
    es = np.mean(pRet[:adn])
    return -VaR, -es

def mc_var_T(returns, method='ABS'): 
    n = 10000
    mean = np.mean(returns)
    # Using a normal distribution
    if method == 'ABS':
        df, loc, scale = t.fit(returns)
        sim_returns = t.rvs(df, loc=loc, scale=scale, size=n)
    else:
        returns_new = returns - mean
        df, loc, scale = t.fit(returns_new)
        sim_returns = t.rvs(df, loc=0, scale=scale, size=n)
    pRet = np.sort(sim_returns)
    
    # Calculate MC VaR
    aup = int(np.ceil(0.05 * n))
    adn = int(np.floor(0.05 * n))
    VaR = (pRet[aup] + pRet[adn]) / 2
    es = np.mean(pRet[:adn])
    return -VaR, -es

def his_var(returns, method='ABS'):
    mean = np.mean(returns)
    # Using a normal distribution
    if method == 'ABS':
        sim_returns = returns
    else:
        returns_new = returns - mean
        sim_returns = returns_new
    pRet = np.sort(sim_returns)
    
    # Calculate MC VaR
    a = int(np.floor(0.05 * len(returns)))
    return -pRet[a]

def var(sim_returns): 
    pRet = np.sort(sim_returns)
    
    # Calculate MC VaR
    aup = int(np.ceil(0.05 * len(pRet)))
    adn = int(np.floor(0.05 * len(pRet)))
    VaR = (pRet[aup] + pRet[adn]) / 2
    es = np.mean(pRet[:adn])
    return -VaR, -es

# A-normal B-T
def copula_simulation(returns, NSim):
    # create the U matrix
    U = np.empty((returns.shape[0], returns.shape[1]))
    ret_A = returns[:, 0]
    ret_B = returns[:, 1]
    mu_A, sigma_A = norm.fit(ret_A)
    nu_B, mu_B, sigma_B = t.fit(ret_B)
    U[:, 0] = norm.cdf(ret_A, loc=mu_A, scale=sigma_A)
    U[:, 1] = t.cdf(ret_B, df=nu_B, loc=mu_B, scale=sigma_B)

    # Transform U into Z
    Z = norm.ppf(U)
    # Spearman correlations of Z
    R_spearman = spearmanr(Z, axis=0)[0]
    cor = np.array([[1, R_spearman], [R_spearman, 1]])
    
    # simulate using the Copula
    # simulate with Spearman Correlation
    #spearman = np.random.multivariate_normal(np.zeros(returns.shape[1]), cor, NSim)
    spearman = simulate_pca(cor, NSim)
    spearman[:, 0] = norm.ppf(norm.cdf(spearman[:, 0]), loc=mu_A, scale=sigma_A)
    spearman[:, 1] = t.ppf(norm.cdf(spearman[:, 1]), df=nu_B, loc=mu_B, scale=sigma_B)
    return spearman

# Compare two arrays with the precision of 1e-8
def compare(a1, a2, precision):
    if isinstance(a1, pd.DataFrame):
        a1_rounded = a1.round(precision)
        a2_rounded = a2.round(precision)
        if a1_rounded.equals(a2_rounded):
            return True
        else:
            return False
    else:
        a1_rounded = np.round(a1, decimals=precision)
        a2_rounded = np.round(a2, decimals=precision)
        if np.allclose(a1_rounded, a2_rounded, atol=1e-8):
            return True
        else:
            return False

def pass_test(a1, a2, test_name, precision = 8):
    if compare(a1, a2, precision):
        print("Test {} passed.".format(test_name))
    else:
        print("Test {} failed.".format(test_name))
        print("Output is:\n{}\nAnswer is:\n{}\n".format(a1, a2))
 
# Test

# # 1.1 Skip Missing rows - Covariance
# test1_1 = pd.read_csv("testfiles/data/test1.csv")
# output1_1 = missing_cov(test1_1.values, skipMiss=True)
# answer1_1 = pd.read_csv("testfiles/data/testout_1.1.csv")
# pass_test(output1_1, answer1_1.values, "1.1")

# # 1.2 Skip Missing rows - Correlation
# test1_2 = pd.read_csv("testfiles/data/test1.csv")
# output1_2 = missing_cov(test1_2.values, skipMiss=True, fun=np.corrcoef)
# answer1_2 = pd.read_csv("testfiles/data/testout_1.2.csv")
# pass_test(output1_2, answer1_2.values, "1.2")

# # 1.3 Pairwise - Covariance
# test1_3 = pd.read_csv("testfiles/data/test1.csv")
# output1_3 = missing_cov(test1_3.values, skipMiss=False)
# answer1_3 = pd.read_csv("testfiles/data/testout_1.3.csv")
# pass_test(output1_3, answer1_3.values, "1.3")

# # 1.4 Pairwise - Correlation
# test1_4 = pd.read_csv("testfiles/data/test1.csv")
# output1_4 = missing_cov(test1_4.values, skipMiss=False, fun=np.corrcoef)
# answer1_4 = pd.read_csv("testfiles/data/testout_1.4.csv")
# pass_test(output1_4, answer1_4.values, "1.4")

# # 2.1 EW Covariance λ=0.97
# test2_1 = pd.read_csv("testfiles/data/test2.csv")
# output2_1 = ewCovar(test2_1.values, 0.97)
# answer2_1 = pd.read_csv("testfiles/data/testout_2.1.csv")
# pass_test(output2_1, answer2_1.values, "2.1")

# # 2.2 EW Correlation λ=0.94
# test2_2 = pd.read_csv("testfiles/data/test2.csv")
# output2_2 = ewCor(test2_2.values, 0.94)
# answer2_2 = pd.read_csv("testfiles/data/testout_2.2.csv")
# pass_test(output2_2, answer2_2.values, "2.2")

# # 2.3 EW Cov w/ EW Var(λ=0.97) EW Correlation(λ=0.94)
# test2_3 = pd.read_csv("testfiles/data/test2.csv")
# output2_3 = ewCovarCor(test2_3.values, 0.97, 0.94)
# answer2_3 = pd.read_csv("testfiles/data/testout_2.3.csv")
# pass_test(output2_3, answer2_3.values, "2.3")
    
# # 3.1 near_psd covariance
# test3_1 = pd.read_csv("testfiles/data/testout_1.3.csv")
# output3_1 = near_psd(test3_1.values)
# answer3_1 = pd.read_csv("testfiles/data/testout_3.1.csv")
# pass_test(output3_1, answer3_1.values, "3.1")

# # 3.2 near_psd Correlation
# test3_2 = pd.read_csv("testfiles/data/testout_1.4.csv")
# output3_2 = near_psd(test3_2.values)
# answer3_2 = pd.read_csv("testfiles/data/testout_3.2.csv")
# pass_test(output3_2, answer3_2.values, "3.2")

# # 3.3 Higham covariance
# test3_3 = pd.read_csv("testfiles/data/testout_1.3.csv")
# output3_3 = higham_nearestPSD(test3_3.values)
# answer3_3 = pd.read_csv("testfiles/data/testout_3.3.csv")
# pass_test(output3_3, answer3_3.values, "3.3")

# # 3.4 Higham Correlation
# test3_4 = pd.read_csv("testfiles/data/testout_1.4.csv")
# output3_4 = higham_nearestPSD(test3_4.values)
# answer3_4 = pd.read_csv("testfiles/data/testout_3.4.csv")
# pass_test(output3_4, answer3_4.values, "3.4")

# # 4.1 cholesky factorization
# test4_1 = pd.read_csv("testfiles/data/testout_3.1.csv")
# output4_1 = chol_psd(test4_1.values)
# answer4_1 = pd.read_csv("testfiles/data/testout_4.1.csv")
# pass_test(output4_1, answer4_1.values, "4.1")

# # 5.1 PD Input
# test5_1 = pd.read_csv("testfiles/data/test5_1.csv")
# sim5_1 = simulate_normal(100000, test5_1.values)
# output5_1 = np.cov(sim5_1, rowvar=False)
# answer5_1 = pd.read_csv("testfiles/data/testout_5.1.csv")
# pass_test(output5_1, answer5_1.values, "5.1", 2)

# # 5.2 PSD Input
# test5_2 = pd.read_csv("testfiles/data/test5_2.csv")
# sim5_2 = simulate_normal(100000, test5_2.values)
# output5_2 = np.cov(sim5_2, rowvar=False)
# answer5_2 = pd.read_csv("testfiles/data/testout_5.2.csv")
# pass_test(output5_2, answer5_2.values, "5.2", 2)

# # 5.3 nonPSD Input, near_psd fix
# test5_3 = pd.read_csv("testfiles/data/test5_3.csv")
# covar5_3 = near_psd(test5_3.values)
# sim5_3 = simulate_normal(100000, covar5_3)
# output5_3 = np.cov(sim5_3, rowvar=False)
# answer5_3 = pd.read_csv("testfiles/data/testout_5.3.csv")
# pass_test(output5_3, answer5_3.values, "5.3", 2)

# # 5.4 nonPSD Input Higham Fix
# test5_4 = pd.read_csv("testfiles/data/test5_3.csv")
# covar5_4 = higham_nearestPSD(test5_4.values)
# sim5_4 = simulate_normal(100000, covar5_4)
# output5_4 = np.cov(sim5_4, rowvar=False)
# answer5_4 = pd.read_csv("testfiles/data/testout_5.4.csv")
# pass_test(output5_4, answer5_4.values, "5.4", 2)

# # 5.5 PSD Input - PCA Simulation
# test5_5 = pd.read_csv("testfiles/data/test5_2.csv")
# sim5_5 = simulate_pca(test5_5.values, 100000, 0.99)
# output5_5 = np.cov(sim5_5, rowvar=False)
# answer5_5 = pd.read_csv("testfiles/data/testout_5.5.csv")
# pass_test(output5_5, answer5_5.values, "5.5", 2)

# # 6.1 Arithmetic returns
# test6_1 = pd.read_csv("testfiles/data/test6.csv")
# output6_1 = return_calculate(test6_1, "DISCRETE", "Date")
# answer6_1 = pd.read_csv("testfiles/data/test6_1.csv")
# pass_test(output6_1, answer6_1, "6.1")

# # 6.2 Log returns
# test6_2 = pd.read_csv("testfiles/data/test6.csv")
# output6_2 = return_calculate(test6_2, "LOG", "Date")
# answer6_2 = pd.read_csv("testfiles/data/test6_2.csv")
# pass_test(output6_2, answer6_2, "6.2")

# # 7.1 Fit Normal Distribution
# test7_1 = pd.read_csv("testfiles/data/test7_1.csv")
# mu7_1, sigma7_1 = norm.fit(test7_1.values)
# output7_1 = pd.DataFrame({'mu':[mu7_1], 'sigma':[sigma7_1]})
# answer7_1 = pd.read_csv("testfiles/data/testout7_1.csv")
# pass_test(output7_1, answer7_1, "7.1", 3)

# # 7.2 Fit TDist
# test7_2 = pd.read_csv("testfiles/data/test7_2.csv")
# nu7_2, mu7_2, sigma7_2 = t.fit(test7_2.values)
# output7_2 = pd.DataFrame({'mu':[mu7_2], 'sigma':[sigma7_2], 'nu':[nu7_2]})
# answer7_2 = pd.read_csv("testfiles/data/testout7_2.csv")
# pass_test(output7_2, answer7_2, "7.2", 3)

# # 7.3 Fit T Regression
# test7_3 = pd.read_csv("testfiles/data/test7_3.csv")
# x7_3 = test7_3[['x1', 'x2', 'x3']].values
# y7_3 = test7_3[['y']].values
# nu7_3, mu7_3, sigma7_3, model7_3 = fit_regression_t(y7_3, x7_3)
# Alpha7_3 = model7_3.beta[0]
# b1 = model7_3.beta[1]
# b2 = model7_3.beta[2]
# b3 = model7_3.beta[3]
# output7_3 = pd.DataFrame({'mu':[mu7_3], 'sigma':[sigma7_3], 'nu':[nu7_3], 'Alpha':[Alpha7_3], 'B1':[b1], 'B2':[b2], 'B3':[b3]})
# answer7_3 = pd.read_csv("testfiles/data/testout7_3.csv")
# pass_test(output7_3, answer7_3, "7.3", 2)

# # 8.1 VaR Normal
# test8_1 = pd.read_csv("testfiles/data/test7_1.csv")
# mu8_1, sigma8_1 = norm.fit(test8_1.values)
# error_model8_1 = norm(loc=mu8_1, scale=sigma8_1)
# varAbs8_1 = -error_model8_1.ppf(0.05)
# varMean8_1 = -(-varAbs8_1 - mu8_1)
# output8_1 = pd.DataFrame({'VaR Absolute':[varAbs8_1], 'VaR Diff from Mean':[varMean8_1]})
# answer8_1 = pd.read_csv("testfiles/data/testout8_1.csv")
# pass_test(output8_1, answer8_1, "8.1", 3)

# # 8.2 VaR TDist
# test8_2 = pd.read_csv("testfiles/data/test7_2.csv")
# nu8_2, mu8_2, sigma8_2 = t.fit(test8_2.values)
# error_model8_2 = t(df=nu8_2, loc=mu8_2, scale=sigma8_2)
# varAbs8_2 = -error_model8_2.ppf(0.05)
# varMean8_2 = -(-varAbs8_2 - mu8_2)
# output8_2 = pd.DataFrame({'VaR Absolute':[varAbs8_2], 'VaR Diff from Mean':[varMean8_2]})
# answer8_2 = pd.read_csv("testfiles/data/testout8_2.csv")
# pass_test(output8_2, answer8_2, "8.2", 3)

# # 8.3 VaR Simulation
# test8_3 = pd.read_csv("testfiles/data/test7_2.csv")
# varAbs8_3, _ = mc_var_T(test8_3['x1'].values, 'ABS')
# varMean8_3, _ = mc_var_T(test8_3['x1'].values, 'MEAN')
# output8_3 = pd.DataFrame({'VaR Absolute':[varAbs8_3], 'VaR Diff from Mean':[varMean8_3]})
# answer8_3 = pd.read_csv("testfiles/data/testout8_3.csv")
# pass_test(output8_3, answer8_3, "8.3", 1)

# # 8.4 ES Normal
# test8_4 = pd.read_csv("testfiles/data/test7_1.csv")
# mu8_4, sigma8_4 = norm.fit(test8_4.values)
# error_model8_4 = norm(loc=mu8_4, scale=sigma8_4)
# varAbs8_4 = -error_model8_4.ppf(0.05)
# def integrand(t):
#     return t * error_model8_4.pdf(t)
# esAbs8_4, _ = quad(integrand, -np.inf, -varAbs8_4)
# esAbs8_4 = -1/0.05 * esAbs8_4
# esMean8_4 = -(-esAbs8_4 - mu8_4)
# output8_4 = pd.DataFrame({'ES Absolute':[esAbs8_4], 'ES Diff from Mean':[esMean8_4]})
# answer8_4 = pd.read_csv("testfiles/data/testout8_4.csv")
# pass_test(output8_4, answer8_4, "8.4", 3)

# # 8.5 ES TDist
# test8_5 = pd.read_csv("testfiles/data/test7_2.csv")
# nu8_5, mu8_5, sigma8_5 = t.fit(test8_5.values)
# error_model8_5 = t(df=nu8_5, loc=mu8_5, scale=sigma8_5)
# varAbs8_5 = -error_model8_5.ppf(0.05)
# def integrand(t):
#     return t * error_model8_5.pdf(t)
# esAbs8_5, _ = quad(integrand, -np.inf, -varAbs8_5)
# esAbs8_5 = -1/0.05 * esAbs8_5
# esMean8_5 = -(-esAbs8_5 - mu8_5)
# output8_5 = pd.DataFrame({'ES Absolute':[esAbs8_5], 'ES Diff from Mean':[esMean8_5]})
# answer8_5 = pd.read_csv("testfiles/data/testout8_5.csv")
# pass_test(output8_5, answer8_5, "8.5", 3)

# # 8.6 VaR Simulation
# test8_6 = pd.read_csv("testfiles/data/test7_2.csv")
# _, esAbs8_6 = mc_var_T(test8_6['x1'].values, 'ABS')
# _, esMean8_6 = mc_var_T(test8_6['x1'].values, 'MEAN')
# output8_6 = pd.DataFrame({'ES Absolute':[esAbs8_6], 'ES Diff from Mean':[esMean8_6]})
# answer8_6 = pd.read_csv("testfiles/data/testout8_6.csv")
# pass_test(output8_6, answer8_6, "8.6", 1)

# # 9.1 Copula
# returns = pd.read_csv("testfiles/data/test9_1_returns.csv")
# holdings = {'A': 100, 'B': 100}
# holdings_nm = list(holdings.keys())
# return_holding = returns[holdings_nm].values

# # Simulate returns
# n = 100000
# sim_returns = copula_simulation(return_holding, n)
# sim_returns = pd.DataFrame(sim_returns)
# sim_returns = sim_returns.rename(columns={0: 'A', 1: 'B'})

# portfolio = pd.DataFrame({'Stock': ['A', 'B'], 'currentValue': [2000.0, 3000.0]})
# iteration = pd.DataFrame({'iteration': [i for i in range(1, n+1)]})
# values = pd.merge(portfolio, iteration, how='cross')
# nv = len(values)
# pnl = np.zeros(nv)
# simulatedValue = np.zeros(nv)
# for i in range(nv):
#     stock = values.iloc[i]['Stock']
#     simulatedValue[i] = values.iloc[i]['currentValue'] * (1 + sim_returns.iloc[values.iloc[i]['iteration']-1][stock])
#     pnl[i] = simulatedValue[i] - values.iloc[i]['currentValue']

# values['pnl'] = pnl # profit and loss
# values['simulatedValue'] = simulatedValue

# output9_1 = pd.DataFrame(columns=['Stock', 'VaR95', 'ES95', 'VaR95_Pct', 'ES95_Pct'])
# for stock in ['A', 'B', 'Total']:
#     if stock == 'Total':
#         var_temp, es_temp = var(values.loc[values['Stock']== 'A', 'pnl'].values+values.loc[values['Stock']== 'B', 'pnl'].values)
#         value_temp = portfolio['currentValue'].sum()
#     else:
#         var_temp, es_temp = var(values.loc[values['Stock']== stock, 'pnl'])
#         value_temp = portfolio.loc[portfolio['Stock']== stock, 'currentValue'].item()
#     var_temp_pct = var_temp / value_temp
#     es_temp_pct = es_temp / value_temp
#     new_row = {'Stock': stock, 'VaR95': var_temp, 'ES95': es_temp, 'VaR95_Pct': var_temp_pct, 'ES95_Pct': es_temp_pct}
#     output9_1.loc[len(output9_1)] = new_row

# answer9_1 = pd.read_csv("testfiles/data/testout9_1.csv")
# pass_test(output9_1, answer9_1, "9.1", -1)
'''
Output is:
   Stock       VaR95        ES95  VaR95_Pct  ES95_Pct
0      A   94.000633  117.709434   0.047000  0.058855
1      B  108.767013  153.677874   0.036256  0.051226
2  Total  153.294575  201.489240   0.030659  0.040298
Answer is:
   Stock       VaR95        ES95  VaR95_Pct  ES95_Pct
0      A   94.460376  118.289371   0.047230  0.059145
1      B  107.880427  151.218174   0.035960  0.050406
2  Total  152.565684  199.704532   0.030513  0.039941
'''