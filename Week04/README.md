All the libraries you need are listed on the first part of the code.
Here also provide the libraries:

* import pandas as pd
* import numpy as np
* from numpy.random import default_rng
* from scipy.stats import t
* from scipy.optimize import minimize
* from statsmodels.tsa.arima.model import ARIMA

Regardless of which question's code you intend to execute, you need to first run the code in the first section of the Jupyter notebook to import the libraries.