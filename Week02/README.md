All the libraries you need are listed on the first part of the code.

Here also provide the libraries:
* pandas
* numpy
* scipy
* statsmodels
* matplotlib

Regardless of which question's code you intend to execute, you need to first run the code in the first section of the Jupyter notebook to import the libraries.
